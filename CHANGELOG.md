# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [unreleased]

## [0.1.0] - 2018-JUN-23
### Added
- init command to CLI
- bump command to CLI

### Changed (not fixed)
-

### Removed
-

### Fixed
-

