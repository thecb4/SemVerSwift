[//]: # "This actually is the most platform independent comment"

# SemVerKit

Use of [Semantic Version](https://semver.org) has become the standard for annotating the evolution of a project. As developers create libraries they often support multiple installaton processes with scripts that require version information. The goal of this package was to accomplish two things.

1. Allow for management of the version, disconnected from the specific language files being used.
2. Allow for each reference of the version to simply be a target that is revised every time the version file is updated.

## Getting Started

### Prerequisites

You must have swift(https://swift.org) installed to use the package and the command line interface.

### Installing the Command Line Interface


## [Mint](https://github.com/yonaskolb/Mint) 

```
$ mint install thecb4/mint
```

### Installing as dependency


## Swift Package Manager

```swift
.package(url: "https://github.com/thecb4/SemVerSwiftKit.git", .upToNextMinor(from: "0.1.0"))
```

## Carthage

```swift
github "thecb4/SemVerSwiftKit" ~> 0.1.0
```

If you want to run the tests

## Running the tests

```
$ swift test
```

### Usage


## Initialize 

```
$ semverswift init
```

## Bump Version 

```
$ semverswift bump <element>
```

## References

The unique part of SemVerSwift Command Line Interface is the update of references each time the version is bumped. 

See example YAML below

```yaml
major: 0
minor: 53
patch: 0
build: 0
refs:
- source: Tests/Fixtures/refs/Info.plist
  listener: InfoPlist
```

The **refs** section provides a path to a file and the type of listener that will revise the paths contents.

The example above will update the Info.plist file with the version information as appropriate.


## File Format

### YAML
```yaml
major: 0
minor: 53
patch: 0
build: 0
refs:
- source: Tests/Fixtures/refs/Info.plist
  listener: InfoPlist
```

### JSON
```json
{
  "major": 0,
  "minor": 0,
  "patch": 0,
  "build": 0
}
```  

## What's Next?

1. Create additional listeners
2. Allow listeners to use custom patterns

## Built With

* [SwiftCLI](https://github.com/jakeheis/SwiftCLI) - The command line interface tooling
* [PathKit](https://github.com/kylef/PathKit) - File access
* [YAMS](https://github.com/jpsim/Yams) - Use of YAML file format

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [Semantic Versioning](http://semver.org/) for versioning. 
For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Cavelle Benjamin** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc

