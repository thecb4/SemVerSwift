import XCTest

import SemVerSwiftKitTests
import SemVerSwiftTests

var tests = [XCTestCaseEntry]()
tests += SemVerSwiftKitTests.__allTests()
tests += SemVerSwiftTests.__allTests()

XCTMain(tests)
