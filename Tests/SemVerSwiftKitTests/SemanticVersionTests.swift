@testable import SemVerSwiftKit
import Spectre
import XCTest

class SemanticVersionTests: XCTestCase {

    func yamlInput(
        _ major: Int, _ minor: Int, _ patch: Int, _ build: Int = 0,
        _ preRelease: String? = nil, _ meta: String? = nil,
        _ refs: ListenerReferences? = nil
        ) throws -> String {

        let subject = SemanticVersion(
            major: major, minor: minor, patch: patch,
            build: build, preRelease: preRelease, meta: meta,
            refs: refs
        )

        // trusting the library to give properly formated data
        return try subject.encodeYAMLString()

    }

    // TODO: Fix for preRelease element
    func jsonInput(
        _ major: Int, _ minor: Int, _ patch: Int, _ build: Int = 0,
        _ preRelease: String? = nil, _ meta: String? = nil,
        _ refs: ListenerReferences? = nil
        ) throws -> String {

        let subject = SemanticVersion(
            major: major, minor: minor, patch: patch,
            build: build, preRelease: preRelease, meta: meta,
            refs: refs
        )

        // trusting the library to give properly formated data
        return try subject.encodeJSONString()

    }

    func testReadVersionYAMLFormatWithNoMetaData() throws {

        // given
        let input = try yamlInput(1, 1, 1)
        let expectedDescription = "1.1.1"
        let expected = SemanticVersion(major: 1, minor: 1, patch: 1)

        // when
        let actual = try SemanticVersion.decode(yaml: input)

        // then
        XCTAssertEqual(actual, expected, "versions do not match")
        XCTAssertEqual(actual.description, expectedDescription, "meta data does not match")

    }

    func testReadVersionJSONFormatWithNoMetaData() throws {

        // given
        let input = try jsonInput(1, 1, 1)
        let expectedDescription = "1.1.1"
        let expected = SemanticVersion(major: 1, minor: 1, patch: 1)

        // when
        let actual = try SemanticVersion.decode(yaml: input)

        // then
        XCTAssertEqual(actual, expected, "versions do not match")
        XCTAssertEqual(actual.description, expectedDescription, "meta data does not match")

    }

    func testReadVersionYAMLFormatWithMetaData() throws {

        // given
        let input = try yamlInput(1, 1, 1, 0, "alpha")
        let expectedDescription = "1.1.1-alpha"
        let expected = SemanticVersion(major: 1, minor: 1, patch: 1, build: 0, preRelease: "alpha")

        // when
        let actual = try SemanticVersion.decode(yaml: input)

        // then
        XCTAssertEqual(actual, expected, "versions do not match")
        XCTAssertEqual(actual.description, expectedDescription, "meta data does not match")

    }

    func testBumpMajorElement() throws {

        // given
        let input = try yamlInput(0, 1, 1)
        let expectedDescription = "1.0.0"
        let expected = SemanticVersion(major: 1, minor: 0, patch: 0)

        // when
        var actual = try SemanticVersion.decode(yaml: input)
        actual.bump( .major)

        // then
        XCTAssertEqual(actual, expected, "major versions do not match")
        XCTAssertEqual(actual.description, expectedDescription, "meta data does not match")

    }

    func testBumpMinorElement() throws {

        // given
        let input = try yamlInput(0, 1, 1)
        let expectedDescription = "0.2.0"
        let expected = SemanticVersion(major: 0, minor: 2, patch: 0)

        // when
        var actual = try SemanticVersion.decode(yaml: input)
        actual.bump( .minor)

        // then
        XCTAssertEqual(actual, expected, "versions do not match")
        XCTAssertEqual(actual.description, expectedDescription, "meta data does not match")

    }

    func testBumpPatchElement() throws {

        // given
        let input = try yamlInput(0, 1, 1)
        let expectedDescription = "0.1.2"
        let expected = SemanticVersion(major: 0, minor: 1, patch: 2)

        // when
        var actual = try SemanticVersion.decode(yaml: input)
        actual.bump( .patch)

        // then
        XCTAssertEqual(actual, expected, "versions do not match")
        XCTAssertEqual(actual.description, expectedDescription, "meta data does not match")

    }

    func testBumpBuildElement() throws {

        // given
        let input = try yamlInput(0, 1, 1)
        let expectedDescription = "0.1.1"
        let expected = SemanticVersion(major: 0, minor: 1, patch: 1, build: 1)

        // when
        var actual = try SemanticVersion.decode(yaml: input)
        actual.bump( .build)

        // then
        XCTAssertEqual(actual, expected, "versions do not match")
        XCTAssertEqual(actual.description, expectedDescription, "meta data does not match")

    }

    func testWriteJSONString() throws {

        // given
        let expected = SemanticVersion(major: 1, minor: 1, patch: 1)
        let subject  = SemanticVersion(major: 1, minor: 1, patch: 1)

        // when
        let actual  = try SemanticVersion.decode(json: subject.encodeJSONString() )

        // then
        XCTAssertEqual(actual, expected, "versions do not match")

    }

    func testWriteYAMLString() throws {

        // given
        let expected = SemanticVersion(major: 1, minor: 1, patch: 1)
        let subject  = SemanticVersion(major: 1, minor: 1, patch: 1)

        // when
        let actual  = try SemanticVersion.decode(yaml: subject.encodeYAMLString() )

        // then
        XCTAssertEqual(actual, expected, "versions do not match")

    }

    func testDefaultJSONString() throws {

        //given
        let expected = SemanticVersion(major: 0, minor: 1, patch: 0, build: 0)

        //when
        let actual  = try SemanticVersion.decode(json: SemanticVersion.defaultJSONString )

        //then
        XCTAssertEqual(actual, expected, "versions do not match")

    }

    func testVersionRefsDoNotExist() {

        // given
        let subject = SemanticVersion(major: 0, minor: 1, patch: 0, build: 0)

        // when
        let actual = subject.refs

        // then
        XCTAssertNil(actual, "expected to find nil for refs, found something")

    }

    func testVersionRefsDoExist() {

        // given
        let subject = SemanticVersion(
            major: 0, minor: 1, patch: 0, build: 0,
            preRelease: nil, meta: nil,
            refs: [ListenerReference(source: "Info.plist", listener: .InfoPlist)]
        )

        // when
        let actual = subject.refs

        // then
        XCTAssertNotNil(actual, "expected to find nil for refs, found something")

    }

    func testReadVersionYAMLFormatWithWithRefs() throws {

        // given
        let inputArtifact = try yamlInput(0, 1, 0, 0, nil, nil, [ListenerReference(source: "Info.plist", listener: .InfoPlist)])
        print("input artifact = \(inputArtifact)")
        let expected = SemanticVersion(
            major: 0, minor: 1, patch: 0, build: 0,
            preRelease: nil, meta: nil,
            refs: [ListenerReference(source: "Info.plist", listener: .InfoPlist)]
        )

        // when
        let actual = try SemanticVersion.decode(yaml: inputArtifact)

        // then
        XCTAssertEqual(actual, expected, "versions do not match")

    }

    func testReadVersionJSONFormatWithWithRefs() throws {

        // given
        let inputArtifact = try jsonInput(0, 1, 0, 0, nil, nil, [ListenerReference(source: "Info.plist", listener: .InfoPlist)])
        let expected = SemanticVersion(
            major: 0, minor: 1, patch: 0, build: 0,
            preRelease: nil, meta: nil,
            refs: [ListenerReference(source: "Info.plist", listener: .InfoPlist)]
        )

        // when
        let actual = try SemanticVersion.decode(json: inputArtifact)

        // then
        XCTAssertEqual(actual, expected, "versions do not match")

    }

    func testFormatWithReferences() throws {
        let inputArtifact = try yamlInput(0, 1, 0, 0, nil, nil,
                                          [
                                            ListenerReference(source: "Tests/Fixtures/refs/Info.plist", listener: .InfoPlist),
                                            ListenerReference(source: "Tests/Fixtures/refs/Formulae", listener: .InfoPlist)
            ])

        print("\(String(describing: inputArtifact))")

    }

}
