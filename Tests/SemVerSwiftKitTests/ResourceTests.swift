@testable import SemVerSwiftKit
import Spectre
import XCTest

class ResourceTests: XCTestCase {

    func testReadingYAMLFile() throws {

        // given
        let expected = 
        """
        major: 0
        minor: 0
        patch: 0
        build: 0
        """

        // when
        // let actual = try Resource.read(from: "./Tests/Fixtures/read/version.yaml")
        let actual: String = try Resource("./Tests/Fixtures/read/version.yaml").read()

        // then
        XCTAssertEqual(actual, expected, "versions do not match")

    }

    func testReadingJSONFile() throws {

        // given
        let expected = 
        """
        {
          major: 0,
          minor: 0,
          patch: 0,
          build: 0
        }
        """

        // when
        // let actual = try Resource.read(from: "./Tests/Fixtures/read/version.json")
        let actual: String = try Resource("./Tests/Fixtures/read/version.json").read()

        // then
        XCTAssertEqual(actual, expected, "versions do not match")

    }

    func testValidateYAMLFileType() throws {

        // given
        let source = "version.yaml"

        // when
        // let actual = Resource.isValidSemanticVersionFileFormat(source: source)
        let actual = Resource(source).isValidSemanticVersionFileFormat()

        XCTAssertTrue(actual)

    }

    func testValidateYMLFileType() throws {

        // given
        let source = "version.yml"

        // when
        // let actual = Resource.isValidSemanticVersionFileFormat(source: source)
        let actual = Resource(source).isValidSemanticVersionFileFormat()

        XCTAssertTrue(actual)

    }

    func testValidateJSONFileType() throws {

        // given
        let source = "version.json"

        // when
        // let actual = Resource.isValidSemanticVersionFileFormat(source: source)
        let actual = Resource(source).isValidSemanticVersionFileFormat()

        XCTAssertTrue(actual)

    }

    func testValidateTXTFileType() throws {

        // given
        let source = "version.txt"

        // when
        // let actual = Resource.isValidSemanticVersionFileFormat(source: source)
        let actual = Resource(source).isValidSemanticVersionFileFormat()

        XCTAssertFalse(actual)

    }

}
