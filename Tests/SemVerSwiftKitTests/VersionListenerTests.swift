@testable import SemVerSwiftKit
import XCTest

class VersionListenerTests: XCTestCase {

    // it should Update Default Info PList Version
    func testItShouldUpdateDefaultInfoPListVersion() throws {

        // given
        let newVersionArtifact = "1.0.0"

        let defaultInfoPListArtifact =
        """
        <?xml version=\"1.0\" encoding=\"UTF-8\"?>
        <!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
        <plist version=\"1.0\">
        <dict>
          <key>CFBundleDevelopmentRegion</key>
          <string>$(DEVELOPMENT_LANGUAGE)</string>
          <key>CFBundleExecutable</key>
          <string>$(EXECUTABLE_NAME)</string>
          <key>CFBundleIdentifier</key>
          <string>$(PRODUCT_BUNDLE_IDENTIFIER)</string>
          <key>CFBundleInfoDictionaryVersion</key>
          <string>6.0</string>
          <key>CFBundleName</key>
          <string>$(PRODUCT_NAME)</string>
          <key>CFBundlePackageType</key>
          <string>BNDL</string>
          <key>CFBundleShortVersionString</key>
          <string>1.0</string>
          <key>CFBundleVersion</key>
          <string>1</string>
        </dict>
        </plist>
        """

        let expected =
        """
        <?xml version=\"1.0\" encoding=\"UTF-8\"?>
        <!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
        <plist version=\"1.0\">
        <dict>
          <key>CFBundleDevelopmentRegion</key>
          <string>$(DEVELOPMENT_LANGUAGE)</string>
          <key>CFBundleExecutable</key>
          <string>$(EXECUTABLE_NAME)</string>
          <key>CFBundleIdentifier</key>
          <string>$(PRODUCT_BUNDLE_IDENTIFIER)</string>
          <key>CFBundleInfoDictionaryVersion</key>
          <string>6.0</string>
          <key>CFBundleName</key>
          <string>$(PRODUCT_NAME)</string>
          <key>CFBundlePackageType</key>
          <string>BNDL</string>
          <key>CFBundleShortVersionString</key>
          <string>\(newVersionArtifact)</string>
          <key>CFBundleVersion</key>
          <string>0</string>
        </dict>
        </plist>
        """

        let subject = InfoPListListener()

        let semVerArtifact = SemanticVersion(major: 1, minor: 0, patch: 0)

        // when
        let actual = try subject.didUpdate(version: semVerArtifact, for: defaultInfoPListArtifact)

        // then
        XCTAssertEqual(actual, expected, "version info does not match")

    }

    // it shouldUpdatePListSemanticVersion

}
