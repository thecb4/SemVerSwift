import XCTest

extension ResourceTests {
    static let __allTests = [
        ("testReadingJSONFile", testReadingJSONFile),
        ("testReadingYAMLFile", testReadingYAMLFile),
        ("testValidateJSONFileType", testValidateJSONFileType),
        ("testValidateTXTFileType", testValidateTXTFileType),
        ("testValidateYAMLFileType", testValidateYAMLFileType),
        ("testValidateYMLFileType", testValidateYMLFileType)
    ]
}

extension SemanticVersionTests {
    static let __allTests = [
        ("testBumpBuildElement", testBumpBuildElement),
        ("testBumpMajorElement", testBumpMajorElement),
        ("testBumpMinorElement", testBumpMinorElement),
        ("testBumpPatchElement", testBumpPatchElement),
        ("testDefaultJSONString", testDefaultJSONString),
        ("testReadVersionJSONFormatWithNoMetaData", testReadVersionJSONFormatWithNoMetaData),
        ("testReadVersionJSONFormatWithWithRefs", testReadVersionJSONFormatWithWithRefs),
        ("testReadVersionYAMLFormatWithMetaData", testReadVersionYAMLFormatWithMetaData),
        ("testReadVersionYAMLFormatWithNoMetaData", testReadVersionYAMLFormatWithNoMetaData),
        ("testVersionRefsDoExist", testVersionRefsDoExist),
        ("testVersionRefsDoNotExist", testVersionRefsDoNotExist),
        ("testWriteJSONString", testWriteJSONString),
        ("testWriteYAMLString", testWriteYAMLString)
    ]
}

extension StringExtensionsTests {
    static let __allTests = [
        ("testItShouldFindRegularExpressionFromPList", testItShouldFindRegularExpressionFromPList),
        ("testItShouldFindRegularExpressionInText", testItShouldFindRegularExpressionInText),
        ("testItShouldFindSubstringCaseInsensitive", testItShouldFindSubstringCaseInsensitive),
        ("testItShouldFindSubstringCaseSensitive", testItShouldFindSubstringCaseSensitive),
        ("testItShouldReplacePatternWithRevision", testItShouldReplacePatternWithRevision)
    ]
}

extension VersionListenerTests {
    static let __allTests = [
        ("testItShouldUpdateDefaultInfoPListVersion", testItShouldUpdateDefaultInfoPListVersion)
    ]
}

#if !os(macOS)
public func __allTests() -> [XCTestCaseEntry] {
    return [
        testCase(ResourceTests.__allTests),
        testCase(SemanticVersionTests.__allTests),
        testCase(StringExtensionsTests.__allTests),
        testCase(VersionListenerTests.__allTests)
    ]
}
#endif
