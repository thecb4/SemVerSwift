@testable import SemVerSwiftKit
import XCTest

class StringExtensionsTests: XCTestCase {

    // It should find substring
    func testItShouldFindSubstringCaseSensitive() {
        //given state
        let artifact = "Hello, world!"
        let subject1 = "Hello"
        let subject2 = "hello"

        // when behavior
        let actual1 = artifact.contains(subject1)
        let actual2 = artifact.contains(subject2)

        // then outcome
        // should find capitalized
        XCTAssertTrue(actual1, "\(subject1) not found in \(artifact) and should have been")
        XCTAssertFalse(actual2, "\(subject2) found in \(artifact) and should not have been")
    }

    // It should find substring case
    func testItShouldFindSubstringCaseInsensitive() {
        //given state
        let artifact = "Hello, world!"
        let subject1 = "Hello"
        let subject2 = "hello"

        // when behavior
        let actual1 = artifact.containsIgnoringCase(subject1)
        let actual2 = artifact.containsIgnoringCase(subject2)

        // then outcome
        // should find capitalized
        XCTAssertTrue(actual1, "\(subject1) not found in \(artifact) and should have been")
        XCTAssertTrue(actual2, "\(subject2) not found in \(artifact) and should have been")
    }

    // It should find regular expression in text
    func testItShouldFindRegularExpressionInText() {

        // given state
        let subject = "<value>1.1.1</value>"

        // when behavior
        let actual = subject.matches(for: SemanticVersion.Pattern)

        // then outcome
        XCTAssertTrue(!actual.isEmpty, "should have found something")

    }

    func testItShouldFindRegularExpressionFromPList() {

        // given state

        let subject =
        """
        <?xml version="1.0" encoding="UTF-8"?>
        <!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
        <plist version="1.0">
          <dict>
          <key>CFBundleDevelopmentRegion</key>
          <string>$(DEVELOPMENT_LANGUAGE)</string>
          <key>CFBundleExecutable</key>
          <string>$(EXECUTABLE_NAME)</string>
          <key>CFBundleIdentifier</key>
          <string>$(PRODUCT_BUNDLE_IDENTIFIER)</string>
          <key>CFBundleInfoDictionaryVersion</key>
          <string>6.0</string>
          <key>CFBundleName</key>
          <string>$(PRODUCT_NAME)</string>
          <key>CFBundlePackageType</key>
          <string>BNDL</string>
          <key>CFBundleShortVersionString</key>
          <string>1.0.0</string>
          <key>CFBundleVersion</key>
          <string>1</string>
          </dict>
        </plist>
        """

        let pattern =
        """
          <key>CFBundleShortVersionString</key>
          <string>\(SemanticVersion.Pattern)</string>
        """

        // when behavior
        let actual = subject.matches(for: pattern)

        // then outcome
        XCTAssertTrue(!actual.isEmpty, "should have found something")

    }

    func testItShouldReplacePatternWithRevision() {
        // given
        let subject =
        """
        <key>CFBundleShortVersionString</key>
        <string>1.0.0</string>
        """

        // Version Pattern
        let versionPattern = SemanticVersion.Pattern

        // Info.plist Pattern
        let pattern =
        """
        <key>CFBundleShortVersionString</key>
        <string>\(versionPattern)</string>
        """

        // revision
        let expected =
        """
        <key>CFBundleShortVersionString</key>
        <string>2.0.0</string>
        """

        // when
        let actual = subject.replace(pattern, with: expected)

        // then
        XCTAssertEqual(actual, expected, "version info does not match")
    }

}
