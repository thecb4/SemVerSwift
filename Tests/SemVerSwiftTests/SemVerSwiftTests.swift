import PathKit
import SemVerSwiftKit
import ShellOut
import XCTest

class SemVerCommandLineTests: XCTestCase {

    let executable  = "./.build/x86_64-apple-macosx10.10/debug/semverswift"

    func setup() throws {

        let files = [
            "Tests/Fixtures/init/.version.yaml",
            "Tests/Fixtures/init/.version.json",
            "Tests/Fixtures/init/.version"
        ]

        for file in files {
            let path = Path(file)
            if path.exists { try path.delete() }
        }

    }

    func testCommandLineWithNoParams() throws {

        let expectedOutput =
        """

        Usage: semverswift <command> [options]

        Manager project version information based on Semantic Versioning, https://semver.org

        Commands:
          init            Initialize a new version file like https://semver.org
          bump            Bump version: major, minor, patch, or build. https://semver.org
          help            Prints this help information
          version         Prints the current version of this app

        """

        let output = try shellOut(to: "\(executable) --help", at: "./")

        XCTAssertEqual(output, expectedOutput)

    }

    func testInitCommandWithProjectDirParameter() throws {

        // given
        let directory = "Tests/Fixtures/init"
        let file      = ".version.yaml"
        let path      = Path(directory) + Path(file)
        let action    = "init"
        let options   = ["-p", directory].joined(separator: " ")

        // when
        if ( path.exists ) { try path.delete() }
        let output = try shellOut(to: "\(executable) \(action) \(options)", at: "./")
        print(output)

        // then
        XCTAssertTrue(path.exists)

    }

    func testInitCommandWithNameParameter() throws {

        // given
        let directory = "Tests/Fixtures/init"
        let file      = ".version.yaml"
        let path      = Path(directory) + Path(file)
        let action    = "init"
        let options   = ["-p", directory, "-n", file].joined(separator: " ")
        let command   = "\(executable) \(action) \(options)"

        print(command)

        // when
        let output = try shellOut(to: command, at: "./")
        print(output)

        // then
        XCTAssertTrue(path.exists)

    }

    func testInitCommandWithFormatParameterYAML() throws {

        // given
        let directory = "Tests/Fixtures/init"
        let file      = ".version"
        let path      = Path(directory) + Path(file)
        let action    = "init"
        let options   = ["-p", directory, "-n", file, "-f", "yaml"].joined(separator: " ")
        let command   = "\(executable) \(action) \(options)"

        // when
        _ = try shellOut(to: command, at: "./")

        // then
        XCTAssertTrue(path.exists)

    }

    func testInitCommandWithFormatParameterJSON() throws {

        // given
        let directory = "Tests/Fixtures/init"
        let file      = ".version"
        let path      = Path(directory) + Path(file)
        let action    = "init"
        let options   = ["-p", directory, "-n", file, "-f", "json"].joined(separator: " ")
        let command   = "\(executable) \(action) \(options)"

        // when
        _ = try shellOut(to: command, at: "./")

        // then
        XCTAssertTrue(path.exists)

    }

    func testBumpCommandBumpDefault() throws {

        // given
        let directory = "Tests/Fixtures/bump"
        let file      = ".version-default.yaml"
        let path      = Path(directory) + Path(file)
        let action    = "bump"
        let options   = ["-p", directory, "-n", file].joined(separator: " ")
        let command   = "\(executable) \(action) \(options)"
        let beforeSource: String  = try path.read()
        let before = try SemanticVersion.decode(source: beforeSource, format: .yaml)

        // when
        _ = try shellOut(to: command, at: "./")
        let afterSource: String  = try path.read()
        let after = try SemanticVersion.decode(source: afterSource, format: .yaml)

        // then
        XCTAssertTrue(after.major == before.major, "major changed, should have only been patch")
        XCTAssertTrue(after.minor == before.minor, "minor changed, should have only been patch")
        XCTAssertTrue((after.patch - before.patch) == 1, "patched changed, but by more than one")
        XCTAssertTrue(after.build == before.build, "build changed, should have only been patch")

    }

    func testBumpCommandBumpPatch() throws {

        // given
        let directory = "Tests/Fixtures/bump"
        let file      = ".version-patch.yaml"
        let path      = Path(directory) + Path(file)
        let action    = "bump patch"
        let options   = ["-p", directory, "-n", file].joined(separator: " ")
        let command   = "\(executable) \(action) \(options)"
        let beforeSource: String  = try path.read()
        let before = try SemanticVersion.decode(source: beforeSource, format: .yaml)

        // when
        _ = try shellOut(to: command, at: "./")
        let afterSource: String = try path.read()
        let after = try SemanticVersion.decode(source: afterSource, format: .yaml)

        // then
        XCTAssertTrue(after.major == before.major, "major changed, should have only been patch")
        XCTAssertTrue(after.minor == before.minor, "minor changed, should have only been patch")
        XCTAssertTrue((after.patch - before.patch) == 1, "patched changed, but by more than one")
        XCTAssertTrue(after.build == before.build, "build changed, should have only been patch")

    }

    func testBumpCommandBumpMinor() throws {

        // given
        let directory = "Tests/Fixtures/bump"
        let file      = ".version-minor.yaml"
        let path      = Path(directory) + Path(file)
        let action    = "bump minor"
        let options   = ["-p", directory, "-n", file].joined(separator: " ")
        let command   = "\(executable) \(action) \(options)"
        let beforeSource: String = try path.read()
        let before = try SemanticVersion.decode(source: beforeSource, format: .yaml)

        // when
        _ = try shellOut(to: command, at: "./")
        let afterSource: String = try path.read()
        let after = try SemanticVersion.decode(source: afterSource, format: .yaml)

        // then
        XCTAssertTrue(after.major == before.major, "major changed, should have only been patch")
        XCTAssertTrue((after.minor - before.minor) == 1, "minor changed, should have only been patch")
        XCTAssertTrue(after.patch == before.patch, "patched changed, but by more than one")
        XCTAssertTrue(after.build == before.build, "build changed, should have only been patch")

    }

    func testBumpCommandBumpMajor() throws {

        // given
        let directory = "Tests/Fixtures/bump"
        let file      = ".version-major.yaml"
        let path      = Path(directory) + Path(file)
        let action    = "bump major"
        let options   = ["-p", directory, "-n", file].joined(separator: " ")
        let command   = "\(executable) \(action) \(options)"
        let beforeSource: String = try path.read()
        let before = try SemanticVersion.decode(source: beforeSource, format: .yaml)

        // when
        _ = try shellOut(to: command, at: "./")
        let afterSource: String = try path.read()
        let after = try SemanticVersion.decode(source: afterSource, format: .yaml)

        // then
        XCTAssertTrue((after.major - before.major) == 1, "major changed, should have only been patch")
        XCTAssertTrue(after.minor == before.minor, "minor changed, should have only been patch")
        XCTAssertTrue(after.patch == before.patch, "patched changed, but by more than one")
        XCTAssertTrue(after.build == before.build, "build changed, should have only been patch")

    }

    func testBumpCommandBumpBuild() throws {

        // given
        let directory = "Tests/Fixtures/bump"
        let file      = ".version-build.yaml"
        let path      = Path(directory) + Path(file)
        let action    = "bump build"
        let options   = ["-p", directory, "-n", file].joined(separator: " ")
        let command   = "\(executable) \(action) \(options)"
        let beforeSource: String = try path.read()
        let before = try SemanticVersion.decode(source: beforeSource, format: .yaml)

        // when
        _ = try shellOut(to: command, at: "./")
        let afterSource: String = try path.read()
        let after = try SemanticVersion.decode(source: afterSource, format: .yaml)

        // then
        XCTAssertTrue(after.major == before.major, "major changed, should have only been patch")
        XCTAssertTrue(after.minor == before.minor, "minor changed, should have only been patch")
        XCTAssertTrue(after.patch == before.patch, "patched changed, but by more than one")
        XCTAssertTrue((after.build - before.build) == 1, "build changed, should have only been patch")

    }

    func testBumpCommandBumpMinorWithReferences() throws {

        // given
        let directory = "Tests/Fixtures/refs"
        let file      = ".version.yaml"
        let path      = Path(directory) + Path(file)
        let action    = "bump minor"
        let options   = ["-p", directory, "-n", file].joined(separator: " ")
        let command   = "\(executable) \(action) \(options)"
        let beforeSource: String = try path.read()
        let before = try SemanticVersion.decode(source: beforeSource, format: .yaml)

        // when
        let output = try shellOut(to: command, at: "./")
        print(output)
        let afterSource: String = try path.read()
        let after = try SemanticVersion.decode(source: afterSource, format: .yaml)

        // then
        XCTAssertTrue(after.major == before.major, "major changed, should have only been patch")
        XCTAssertTrue((after.minor - before.minor) == 1, "minor should have changed by 1, but didn't")
        XCTAssertTrue(after.patch == before.patch, "patched changed, but by more than one")
        XCTAssertTrue(after.build == before.build, "build changed, should have only been patch")

    }

}
