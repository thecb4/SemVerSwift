import XCTest

extension SemVerCommandLineTests {
    static let __allTests = [
        ("testCommandLineWithNoParams", testCommandLineWithNoParams)
    ]
}

#if !os(macOS)
public func __allTests() -> [XCTestCaseEntry] {
    return [
        testCase(SemVerCommandLineTests.__allTests)
    ]
}
#endif
