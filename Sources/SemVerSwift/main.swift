import Foundation
import SemVerSwiftCLI

let version = "0.1.0"

let name = "semverswift"

let description = "Manager project version information based on Semantic Versioning, https://semver.org"

let commands: [Command] = [InitCommand(), BumpCommand()]

let worker = CommandLineInterface(name: name, version: version, description: description, commands: commands)

let result = worker.go()

if result != 0 { _ = worker.go(with: []) }
