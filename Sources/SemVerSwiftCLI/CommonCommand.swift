import SemVerSwiftKit
import SwiftCLI

public typealias BumpElement = SemanticVersion.Element

public typealias SemanticVersionFileFormat = Resource.SemanticVersionFileFormat

public class CommonCommand: Command {

    public var name = "common"

    public var shortDescription = "Common Command, will not work"

    public let defaultSemanticVersionFileFormat: SemanticVersionFileFormat = .yaml

    public let defaultDirectory: String = "./"

    public let defaultFileName: String = ".version.yaml"

    public let defaultFormat: SemanticVersionFileFormat = .yaml

    public var projectDirKey: Key<String> = Key<String>("-p", "--project-dir", description: "project directory to init")

    public var nameKey: Key<String> = Key<String>("-n", "--name", description: "name of the version file [default = .version.yaml]")

    public var formatKey: Key<String> = Key<String>("-f", "--format", description: "if name doesn't have type, specify with format [yaml or json]")

    public init(name: String, shortDescription: String) {
        self.name = name
        self.shortDescription = shortDescription
    }

    public func execute() throws { }

    func chooseSemanticVersionFileFormat(_ fromSource: SemanticVersionFileFormat?, _ fromKey: SemanticVersionFileFormat?) -> SemanticVersionFileFormat {

        switch (fromSource, fromKey) {

        case (.some(let sourceFormat), .none):
            print("source format = \(sourceFormat), will use format from source \(sourceFormat)")
            return sourceFormat

        case (.none, .some(let keyFormat)):
            print("no source format found, using keyed format \(keyFormat)")
            return keyFormat

        case (.some(let sourceFormat), .some(let keyFormat)):
            if sourceFormat == keyFormat {
                print("source format = \(sourceFormat), key format = \(keyFormat)")
                return sourceFormat
            } else {
                print("File and Key do not match, source format = \(sourceFormat), key format = \(keyFormat). Using Source Format")
                return sourceFormat
            }

        default:
            print("using default format = \(defaultFormat)")
            return defaultFormat
        }

    }

}
