import SemVerSwiftKit
import SwiftCLI

public class BumpCommand: CommonCommand {

    public let bumpElementParameter =  OptionalParameter()

    public init() {
        super.init(
            name: "bump",
            shortDescription: "Bump version: major, minor, patch, or build. https://semver.org"
        )
    }

    public override func execute() throws {

        let elementString = bumpElementParameter.value ?? "patch"

        guard let element = BumpElement(rawValue: elementString) else {
            throw CommandLineError.badBumpType
        }

        let dir = projectDirKey.value ?? defaultDirectory

        let name = nameKey.value ?? defaultFileName

        // let path = [dir, name].joined(separator: "/")

        let path = Resource(dir) + Resource(name)

        let formatFromSource = Resource(name).semanticVersionFileFormat

        let formatFromKey    = Resource.SemanticVersionFileFormat(rawValue: formatKey.value)

        let format = chooseSemanticVersionFileFormat(formatFromSource, formatFromKey)

        // let source  = try Resource.read(from: path)
        let source: String = try path.read()

        var version = try SemanticVersion.decode(source: source, format: format)

        version.bump(element)

        print("references = \(String(describing: version.refs))")

        if let refs = version.refs {
            for ref in refs {
                print("ref = \(ref)")
                let resource = Resource(ref.source)
                let content: String = try resource.read()
                let revision = try ref.didUpdate(version: version, for: content)
                try resource.write(revision)
            }
        }

        stdout <<<
        """
        bumping \(element)
        """

        try path.write(version, with: format)

    }

}
