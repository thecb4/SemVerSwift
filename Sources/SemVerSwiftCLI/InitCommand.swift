import SemVerSwiftKit
import SwiftCLI

public class InitCommand: CommonCommand {

    // public let name = "init"

    // public let shortDescription = "Initialize a new version file like https://semver.org"

    public init() {
        super.init(
            name: "init",
            shortDescription: "Initialize a new version file like https://semver.org"
        )
    }

    public override func execute() throws {

        let dir = self.projectDirKey.value ?? defaultDirectory

        let name = nameKey.value ?? defaultFileName

        // let path = [dir, name].joined(separator: "/")
        let path = Resource(dir) + Resource(name)

        stdout <<< "Initializing new version file in \(dir)"

        let formatFromSource = Resource(name).semanticVersionFileFormat

        let formatFromKey    = Resource.SemanticVersionFileFormat(rawValue: formatKey.value)

        let format = chooseSemanticVersionFileFormat(formatFromSource, formatFromKey)

        let version = SemanticVersion()

        try path.write(version, with: format)

    }

}
