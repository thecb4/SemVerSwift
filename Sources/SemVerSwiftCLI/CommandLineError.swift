enum CommandLineError: Error {

    case badInitOptions
    case badBumpType

}
