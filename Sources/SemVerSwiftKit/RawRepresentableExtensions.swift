// https://stackoverflow.com/questions/42373485/swift-rawrepresentable-init-with-optional-rawvalue
extension RawRepresentable {

  /**
    Implements an optional init of RawRepresentable. Returns nil if optional is nil

    - parameter rawValue: The optional raw value to be cast

    - returns: RawRepresentable or nil
  */
  public init?(rawValue optionalRawValue: RawValue?) {

    guard let rawValue = optionalRawValue, let value = Self(rawValue: rawValue) else {
        return nil

    }

    self = value
  }
}
