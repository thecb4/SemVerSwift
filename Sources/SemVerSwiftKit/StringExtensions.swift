import Foundation

extension String {

    func contains(_ find: String) -> Bool {

        return self.range(of: find) != nil

    }

    func containsIgnoringCase(_ find: String) -> Bool {

        return self.range(of: find, options: .caseInsensitive) != nil

    }

    func matches(`for` pattern: String) -> [String] {

        do {
            let regex = try NSRegularExpression(pattern: pattern)
            let results = regex.matches(in: self,
                                        range: NSRange(self.startIndex..., in: self))
            return results.map {
                String(self[Range($0.range, in: self)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }

    // syntactic sugar
    func replace(_ pattern: String, with revision: String) -> String {
        return self.replacingOccurrences(of: pattern, with: revision, options: [.regularExpression])
    }

}
