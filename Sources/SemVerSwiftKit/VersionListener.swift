
/// Type alias for an array of ListerReference
public typealias ListenerReferences = [ListenerReference]


/**
  Struct representing a reference to a file that will be updated each time the version file is updated.
*/
public struct ListenerReference: Codable {

    /// source file that will be updated with the revised version information
    public let source: String

    /// the listener that will update the file
    public let listener: VersionListenerType

    /**
      Listener Reference updates referenced file with new Semantic Version

      - parameter version: The Semantic Version to update in the content
      - parameter for: The the content to be revised
    */
    public func didUpdate(version: SemanticVersion, `for` info: String) throws -> String {
        return try listener.didUpdate(version: version, for: info)
    }
}

extension ListenerReference: Equatable {

    /**
      Determines equality of a ListenerReference.

     - parameter lhs: left-hand side of equatable
     - parameter rhs: right-hand side of equatable

     - returns: true if equal

    */
    public static func == (lhs: ListenerReference, rhs: ListenerReference) -> Bool {
        return
            lhs.source   == rhs.source &&
            lhs.listener == rhs.listener
    }

}


/**
  Implementation of this method allows for the version to list references and update the version value in each reference.
*/
public protocol VersionListener {

    /**
     Updates a string of information that carries semantic version information

     - Parameter version: The semantic version being updated
     - Parameter for: The information string to be revised

     - return: String

     - throws: throws an error if the version information update fails in any way
    */
    func didUpdate(version: SemanticVersion, `for` info: String) throws -> String

}

/**
  Version listener types that are allowed
*/
public enum VersionListenerType: String, Codable {

    /// Info.plist type
    case InfoPlist
}

extension VersionListenerType {

    var listener: VersionListener {
        switch self {
        case .InfoPlist:
            return InfoPListListener()
        }
    }

    /**
     Updates the contents with new Smeantic Version data.

     - Parameter version: The semantic version being updated
     - Parameter for: The information string to be revised

     - return: String

     - throws: throws an error if the version information update fails in any way
    */
    public func didUpdate(version: SemanticVersion, `for` info: String) throws -> String {
        return try listener.didUpdate(version: version, for: info)
    }

}
