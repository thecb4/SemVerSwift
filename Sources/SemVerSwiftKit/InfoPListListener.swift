
/**
  Info.plist Listener

  Apple has two elements that can be used by the SemanticVersion struct.

  1. CFBundleShortVersionString: Semantic Version short string
  2. CFBundleVersion: Semantic Version build number

*/
public class InfoPListListener: VersionListener {

    /// InfoPlist Default Version Pattern (not Semantic Version Compliant)
    public static let defaultPattern = "(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)"

    /// InfoPlist Default Version Element
    public static let defaultVersionInfoElement: String =
    """
      <key>CFBundleShortVersionString</key>
      <string>1.0</string>
    """

    /// Public init
    public init() { }

    /**
      Updates the CFBundleShortVersionString element within the Info.plist file

      - parameter version: The Semantic Version to be used to update references
      - parameter for: The Content string to modify

      - returns: Revised content string

      - throws: Error if the content cannot be revused
    */
    func updateCFBundleShortVersionString(version: SemanticVersion, `for` info: String) throws -> String {

        // Info.plist is created using 1.0 instead of 1.0.0
        let versionPattern = isFirstUpdate(of: info) ? InfoPListListener.defaultPattern : SemanticVersion.Pattern

        // Info.plist Pattern
        let pattern =
        """
          <key>CFBundleShortVersionString</key>
          <string>\(versionPattern)</string>
        """

        // revision
        let revision =
        """
          <key>CFBundleShortVersionString</key>
          <string>\(version.shortDescription)</string>
        """

        return info.replace(pattern, with: revision)

    }

    /**
      Updates the CFBundleVersion element within the Info.plist file

      - parameter version: The Semantic Version to be used to update references
      - parameter for: The Content string to modify

      - returns: Revised content string

      - throws
    */
    func updateCFBundleVersion(version: SemanticVersion, `for` info: String) throws -> String {
      // Info.plist is created using 1.0 instead of 1.0.0
      let buildPattern = "([0-9]+)"

      // Info.plist Pattern
      let pattern =
      """
        <key>CFBundleVersion</key>
        <string>\(buildPattern)</string>
      """

      // revision
      let revision =
      """
        <key>CFBundleVersion</key>
        <string>\(version.build)</string>
      """

      return info.replace(pattern, with: revision)
    }

    /**
      Implementation of the VersionListener protocol method

      - parameter version: The Semantic Version to be used to update references
      - parameter for: The Content string to modify

      - returns: Revised content string

      - throws: Error
    */
    public func didUpdate(version: SemanticVersion, `for` info: String) throws -> String {

      let interimRevision = try updateCFBundleShortVersionString(version: version, for: info)

      let finalRevision   = try updateCFBundleVersion(version: version, for: interimRevision)

      return finalRevision

    }

    /**
      Validation of first update. Necessary because default data in Info.plist is not SemVer compliant.

      - parameter of: The content string to see if it's the first time updating.

      - returns: Bool
    */
    func isFirstUpdate(of info: String) -> Bool {
      return info.contains(InfoPListListener.defaultVersionInfoElement)
    }

}
