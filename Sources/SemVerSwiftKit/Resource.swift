import Foundation
@_exported import PathKit

/// Type alias for Path from PathKit
public typealias Resource = Path

extension Path {

  /**
   Semantic Version File Format.

   - yaml,yml: YAML based file formats

   - json: JSON based file formats
   */
  public enum SemanticVersionFileFormat: String {

      /// YAML format with .yaml extension
      case yaml

      /// YAML format with .yml extension
      case yml

      /// JSON format with .json extension
      case json
  }

  /**
   Provides the format of the file based on the file suffix.

   - returns: SemanticVersionFileFormat? nil if the file format is not recognized
  */
  public var semanticVersionFileFormat: SemanticVersionFileFormat? {

    let parts = self.description.split(separator: ".")

    if (parts.count == 1) { return nil }

    let fileExtension = String(describing: parts[1])

    return SemanticVersionFileFormat(rawValue: fileExtension)
  }

  /**
   Provides validation for file format based on 

   - returns: Bool 
  */
  public func isValidSemanticVersionFileFormat() -> Bool {

    guard let _ = self.semanticVersionFileFormat else { return false }

    return true

  }

  /**
   Writes Semantic Version to given file path with a specific format.

   - parameter version: The semantic version to write to file.
   - paremeter with: The file format in which to write the version. 

   - throws: Error if the version cannot be written to file
  */
  public func write(_ version: SemanticVersion, with format: SemanticVersionFileFormat) throws {

    var encoded = ""

    switch format {

      case .yaml, .yml:
          encoded = try version.encodeYAMLString()

      case .json:
          encoded = try version.encodeJSONString()

    }

    try write(encoded)

  }
  
}