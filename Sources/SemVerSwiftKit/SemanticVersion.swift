import Foundation
import Yams

/**
  [Semantic Versioning](https://semver.org) 2.0.0, https://semver.org

  Given a version number MAJOR.MINOR.PATCH, increment the:

  1. MAJOR version when you make incompatible API changes,
  2. MINOR version when you add functionality in a backwards-compatible manner, and
  3. PATCH version when you make backwards-compatible bug fixes.
  4. Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.

 */

public struct SemanticVersion: Codable {

    /**
     Major version X (X.y.z | X > 0) MUST be incremented if any backwards incompatible changes are introduced to the public API.
     It MAY include minor and patch level changes. Patch and minor version MUST be reset to 0 when major version is incremented.
     */
    public var major: Int

    /**
     Minor version Y (x.Y.z | x > 0) MUST be incremented if new, backwards compatible functionality is introduced to the public API.
     It MUST be incremented if any public API functionality is marked as deprecated. It MAY be incremented if substantial new functionality or improvements are introduced within the private code.
     It MAY include patch level changes.
     Patch version MUST be reset to 0 when minor version is incremented.
     */
    public var minor: Int

    /**
     Patch version Z (x.y.Z | x > 0) MUST be incremented if only backwards compatible bug fixes are introduced.
     A bug fix is defined as an internal change that fixes incorrect behavior.
     */
    public var patch: Int

    /**
     Increment project build count. can be used as part of version meta data
     */
    public var build: Int

    /**
     A pre-release version MAY be denoted by appending a hyphen and a series of dot separated identifiers immediately following the patch version.
     Identifiers MUST comprise only ASCII alphanumerics and hyphen [0-9A-Za-z-]. Identifiers MUST NOT be empty. Numeric identifiers MUST NOT include leading zeroes.
     Pre-release versions have a lower precedence than the associated normal version.
     A pre-release version indicates that the version is unstable and might not satisfy the intended compatibility requirements as denoted by its associated normal version.
     Examples: 1.0.0-alpha, 1.0.0-alpha.1, 1.0.0-0.3.7, 1.0.0-x.7.z.92.
     */
    public var preRelease: String?

    /**
     Build metadata MAY be denoted by appending a plus sign and a series of dot separated identifiers immediately following the patch or pre-release version.
     Identifiers MUST comprise only ASCII alphanumerics and hyphen [0-9A-Za-z-]. Identifiers MUST NOT be empty.
     Build metadata SHOULD be ignored when determining version precedence. Thus two versions that differ only in the build metadata, have the same precedence.
     Examples: 1.0.0-alpha+001, 1.0.0+20130313144700, 1.0.0-beta+exp.sha.5114f85.
     */
    public var meta: String?

    /**
     A list of files that will be changed when the version changes.
     */
    public var refs: ListenerReferences?

    /**
     Public init with default values.
     */
    public init(
        major: Int = 0, minor: Int = 1, patch: Int = 0, build: Int = 0,
        preRelease: String? = nil, meta: String? = nil,
        refs: ListenerReferences? = nil
        ) {

        self.major = major
        self.minor = minor
        self.patch = patch
        self.build = build
        self.preRelease = preRelease
        self.meta  = meta
        self.refs  = refs

    }

    /**
     Semantic Version element.

     - major: Major version X (X.y.z | X > 0) MUST be incremented if any backwards incompatible changes are introduced to the public API.
     It MAY include minor and patch level changes. Patch and minor version MUST be reset to 0 when major version is incremented.

     - minor: Minor version Y (x.Y.z | x > 0) MUST be incremented if new, backwards compatible functionality is introduced to the public API.
     It MUST be incremented if any public API functionality is marked as deprecated. It MAY be incremented if substantial new functionality or improvements are introduced within the private code. It MAY include patch level changes. Patch version MUST be reset to 0 when minor version is incremented.

     - patch: Patch version Z (x.y.Z | x > 0) MUST be incremented if only backwards compatible bug fixes are introduced.
     A bug fix is defined as an internal change that fixes incorrect behavior.

     - build: Non-Semantic Version based build number
     */
    public enum Element: String {

        /// Semantic Version Major element.
        case major

        /// Semantic Version Minor element.
        case minor

        /// Semantic Version Patch element.
        case patch

        /// Semantic Version Build element.
        case build

    }

    /**
     Bumps the version element.

     - parameter element: The semantic version element to bump ( major | minor | patch | build ).

     */
    public mutating func bump(_ element: SemanticVersion.Element) {

        switch element {

        case .major:
            major += 1
            minor  = 0
            patch  = 0

        case .minor:
            minor += 1
            patch  = 0

        case .patch:
            patch += 1

        case .build:
            build += 1

        }

    }

}

extension SemanticVersion: CustomStringConvertible {

    /// Fully qualified version string that includes pre-release and meta data.
    public var description: String {

        let preReleaseString = (preRelease == nil) ? "" : "-\(preRelease!)"

        let metaString       = (meta == nil) ? "" : "+\(meta!)"

        return "\(major).\(minor).\(patch)\(preReleaseString)\(metaString)"

    }

    /// Standard Semantic Version of MAJOR.MINOR.PATCH
    public var shortDescription: String {

        return "\(major).\(minor).\(patch)"

    }

}

extension SemanticVersion {

    /**
     Regular expression that represents a cononical Semantic Version

     ([0-9]+)\\.([0-9]+)\\.([0-9]+)(?:(\\-[0-9A-Za-z-]+(?:\\.[0-9A-Za-z-]+)*))?(?:\\+[0-9A-Za-z-\\-\\.]+)?
     */
    public static let Pattern = "([0-9]+)\\.([0-9]+)\\.([0-9]+)(?:(\\-[0-9A-Za-z-]+(?:\\.[0-9A-Za-z-]+)*))?(?:\\+[0-9A-Za-z-\\-\\.]+)?"

    /// Default YAML string during init.
    public static let defaultYamlString = 
    """
    major: 0
    minor: 1
    patch: 0
    build: 0
    """

    /// Default JSON string during init.
    public static let defaultJSONString = 
    """
    {
      \"major\": 0,
      \"minor\": 1,
      \"patch\": 0,
      \"build\": 0
    }
    """
}

/**
 Protocol provides capability to decode YAML or JSON to type.
 */
public protocol StringDecoder {

    /// Resulting Type from decoded string
    associatedtype Decoded

    /**
     Decodes a YAML string.

     - parameter yaml: the yaml string to decode.

     - returns: Decoded type to be returned

     - throws: throws an error if the yaml string cannot be decoded

     */
    static func decode(yaml: String) throws -> Decoded

    /**
      Decodes a JSON string.

      - parameter yaml: the yaml string to decode.

      - throws: throws an error if the yaml string cannot be decoded

    */
    static func decode(json: String) throws -> Decoded

    /**
      Encodes a JSON string.

      - throws: throws an error if the struct cannot be encoded

    */
    func encodeJSONString() throws -> String

    /**
     Encodes a YAML string.

     - throws: throws an error if the struct cannot be encoded

     */
    func encodeYAMLString() throws -> String
}

extension SemanticVersion: StringDecoder {

    /// Decoded as SemanticVersion
    public typealias Decoded = SemanticVersion

    /**
     Decodes a YAML string.

     - parameter yaml: the yaml string to decode.

     - returns: SemanticVersion

     - throws: throws an error if the yaml string cannot be decoded
    */
    public static func decode(yaml: String) throws -> SemanticVersion {

        let decoder = YAMLDecoder()

        let decoded = try decoder.decode(self, from: yaml)

        return decoded

    }

    /**
     Decodes a JSON string.

     - parameter json: the yaml string to decode.

     - returns: SemanticVersion

     - throws: throws an error if the json string cannot be decoded
    */
    public static func decode(json: String) throws -> SemanticVersion {

        let decoder = JSONDecoder()

        let decoded = try decoder.decode(self, from: json.data(using: .utf8)!)

        return decoded
    }

    /**
     Decodes a SemanticVersion struct from a string

     - parameter source: The string to decode
     - parameter format: The format of the file to decode

     - returns: SemanticVersion

     - throws: throws an error if the yaml string cannot be decoded
    */
    public static func decode(source: String, format: Resource.SemanticVersionFileFormat) throws -> SemanticVersion {
        switch format {
        case .yaml, .yml:
            return try SemanticVersion.decode(yaml: source)
        case .json:
            return try SemanticVersion.decode(json: source)

        }
    }

    /**
     Encodes a JSON string.

     - throws: throws an error if the struct cannot be encoded
    */
    public func encodeJSONString() throws -> String {

        let encoder = JSONEncoder()

        // https://benscheirman.com/2017/06/swift-json/

        encoder.outputFormatting = [.prettyPrinted]

        let data    = try encoder.encode(self)

        return String(data: data, encoding: .utf8)!
    }

    /**
     Encodes a YAML string.

     - throws: throws an error if the struct cannot be encoded
    */
    public func encodeYAMLString() throws -> String {
        let encoder = YAMLEncoder()
        return try encoder.encode(self)
    }

}

extension SemanticVersion: Equatable {

    /**
     Determines equality of a SemanticVersion.

     - parameter lhs: left-hand side of equatable
     - parameter rhs: right-hand side of equatable

     - returns: true if equal
    */
    public static func == (lhs: SemanticVersion, rhs: SemanticVersion) -> Bool {
        return
            lhs.major == rhs.major &&
                lhs.minor == rhs.minor &&
                lhs.patch == rhs.patch &&
                lhs.build == rhs.build &&
                lhs.preRelease == rhs.preRelease &&
                lhs.meta  == rhs.meta &&
                lhs.refs  == rhs.refs
    }

}
